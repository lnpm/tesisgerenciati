# README #

Proyecto de tesis.

### TEMA ###

* Implementación de un asistente conversacional virtual con aprendizaje automático para la atención de contribuyentes en la Dirección General de Ingresos de Nicaragua en el año 2022.
* Version 1

### OBJETIVO GENERAL ###

* Elaborar estudio para la implementación de un asistente conversacional virtual con aprendizaje automático para la atención de contribuyentes en la Dirección General de Ingresos de Nicaragua en el año 2022.

### OBJETIVOS ESPECÍFICOS ###

-	Analizar la situación actual de los recursos humanos y tecnológicos del área de asistencia a los contribuyentes de la Dirección General de Ingresos.

-	Identificar los procesos de atención a los contribuyentes y las áreas de conocimientos  a los que responderá el asistente conversacional.

-	Comparar alternativas de tecnología de asistente conversacional de procesos disponible en el mercado.

-	Proponer un plan de implementación para un asistente conversacional virtual con aprendizaje automático para la atención de contribuyentes en la Dirección General de Ingresos.


### UNIVERSIDAD NACIONAL AUTÓNOMA DE NICARAUGUA (UNAN-MANAGUA) ###
### MAESTRÍA EN GERENCIA DE TECNOLOGÍAS DE LA INFORMACIÓN, EMPRENDIMIENTO E INNOVACIÓN ###